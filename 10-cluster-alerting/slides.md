%title: VictoriaMetrics
%author: xavki


█╗   ██╗██╗ ██████╗████████╗ ██████╗ ██████╗ ██╗ █████╗ 
██║   ██║██║██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗██║██╔══██╗
██║   ██║██║██║        ██║   ██║   ██║██████╔╝██║███████║
╚██╗ ██╔╝██║██║        ██║   ██║   ██║██╔══██╗██║██╔══██║
 ╚████╔╝ ██║╚██████╗   ██║   ╚██████╔╝██║  ██║██║██║  ██║
  ╚═══╝  ╚═╝ ╚═════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚═╝╚═╝  ╚═╝
███╗   ███╗███████╗████████╗██████╗ ██╗ ██████╗███████╗  
████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║██╔════╝██╔════╝  
██╔████╔██║█████╗     ██║   ██████╔╝██║██║     ███████╗  
██║╚██╔╝██║██╔══╝     ██║   ██╔══██╗██║██║     ╚════██║  
██║ ╚═╝ ██║███████╗   ██║   ██║  ██║██║╚██████╗███████║  
╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚═╝ ╚═════╝╚══════╝  
                                                         


-----------------------------------------------------------------------

# VICTORIA METRICS : VMAlert & Alertmanager

<br>

* vmalert - some variables

```
VERSION=1.91.3
IP=$(hostname -I | awk '{print $2}')
VERSION_ALERTMANAGER=0.26.0
USER_APP="victoriametrics"
ALERTMANAGER_PEER="vmagent2"
```

-----------------------------------------------------------------------

# VICTORIA METRICS : VMAlert & Alertmanager

<br>

* vmalert - install binary if needed

```
wget -qq https://github.com/VictoriaMetrics/VictoriaMetrics/releases/download/v${VERSION}/vmutils-linux-amd64-v${VERSION}.tar.gz
tar xzf vmutils-linux-amd64-v${VERSION}.tar.gz -C /usr/local/bin/
chmod -R +x /usr/local/bin/
groupadd --system ${USER_APP}
useradd -s /sbin/nologin --system -g ${USER_APP} ${USER_APP}
```

-----------------------------------------------------------------------

# VICTORIA METRICS : VMAlert & Alertmanager

<br>

* vmalert - create the conf directory

```
mkdir -p mkdir -p /etc/vmalert/
chown -R ${USER_APP}:${USER_APP} /etc/vmalert/
```

-----------------------------------------------------------------------

# VICTORIA METRICS : VMAlert & Alertmanager

<br>

* vmalert - create the systemd service

```
echo "
[Unit]
Description=Description=VictoriaAlert service
After=network.target

[Service]
Type=simple
User=${USER_APP}
ExecStart=/usr/local/bin/vmalert-prod \
      -datasource.url=http://127.0.0.1:8427/ \
      -remoteRead.url=http://127.0.0.1:8427/ \
      -remoteWrite.url=http://127.0.0.1:8427/ \
      -notifier.url=http://vmagent1:9093 \
      -notifier.url=http://vmagent2:9093 \
      -rule=/etc/vmalert/*.yml \
      -external.url=http://graf1:3000

SyslogIdentifier=vmalert
Restart=always

[Install]
WantedBy=multi-user.target
" >/etc/systemd/system/vmalert.service
```

-----------------------------------------------------------------------

# VICTORIA METRICS : VMAlert & Alertmanager

<br>

* vmalert - define some sample rules

```
echo "
groups:
- name: node_exporter_alerts
  rules:
  - alert: HostHighCpuLoad
    expr: 100 - (avg by(instance) (rate(node_cpu_seconds_total{mode=\"idle\"}[2m])) * 100) > 80
    for: 0m
    labels:
      severity: warning
    annotations:
      summary: Host high CPU load (instance {{ \$labels.instance }})
      description: CPU load is > 80%\n  VALUE = {{ \$value }}
  - alert: Node down
    expr: up{job=\"node_exporter\"} == 0
    for: 2m
    labels:
      severity: warning
    annotations:
      title: Node {{ \$labels.instance }} is down
      description: Failed to scrape {{ \$labels.job }} on {{ \$labels.instance }} for more than 2 minutes. Node seems down.
" >> /etc/vmalert/node_exporter.yml
```

-----------------------------------------------------------------------

# VICTORIA METRICS : VMAlert & Alertmanager

<br>

* vmalert - start and enable

```
systemctl restart vmalert
systemctl enable vmalert
```

-----------------------------------------------------------------------

# VICTORIA METRICS : VMAlert & Alertmanager

<br>

* alertmanager - create user and directory

```
useradd --no-create-home --shell /bin/false alertmanager
mkdir -p /etc/alertmanager  /var/lib/alertmanager
chown -R alertmanager:alertmanager /var/lib/alertmanager/ /etc/alertmanager
```

-----------------------------------------------------------------------

# VICTORIA METRICS : VMAlert & Alertmanager

<br>

* alertmanager - install binary

```
wget -q https://github.com/prometheus/alertmanager/releases/download/v${VERSION_ALERTMANAGER}/alertmanager-${VERSION_ALERTMANAGER}.linux-amd64.tar.gz
tar xzf alertmanager-${VERSION_ALERTMANAGER}.linux-amd64.tar.gz
cp alertmanager-${VERSION_ALERTMANAGER}.linux-amd64/alertmanager /usr/local/bin/
cp alertmanager-${VERSION_ALERTMANAGER}.linux-amd64/amtool /usr/local/bin/
chmod +x /usr/local/bin/*
```

-----------------------------------------------------------------------

# VICTORIA METRICS : VMAlert & Alertmanager

<br>

* alertmanager - create a sample sender

```
echo "
global:
  resolve_timeout: 2m
  smtp_require_tls: false
route:
  group_by: ['instance', 'severity']
  group_wait: 10s		# temps d'attente avant notification du group
  group_interval: 1m		# délai par rapport aux alertes du même groupe
  repeat_interval: 30s		# attente avant répétition
  receiver: 'email-me'
receivers:
  - name: 'null'
  - name: 'email-me'
    email_configs:
    - to: 'xxx@moi.fr'
      from: 'yyy@moi.fr'
      smarthost: '127.0.0.1:2525'
```

-----------------------------------------------------------------------

# VICTORIA METRICS : VMAlert & Alertmanager

<br>

* alertmanager - install the systemd service

```
echo "
[Unit]
Description=Alertmanager
Wants=network-online.target
After=network-online.target

[Service]
User=alertmanager
Group=alertmanager
Type=simple
WorkingDirectory=/etc/alertmanager/
ExecStart=/usr/local/bin/alertmanager \
      --storage.path=/var/lib/alertmanager/ \
      --config.file=/etc/alertmanager/alertmanager.yml \
      --cluster.peer=${ALERTMANAGER_PEER}:9094 \
      --web.external-url http://0.0.0.0:9093

[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/alertmanager.service
```

-----------------------------------------------------------------------

# VICTORIA METRICS : VMAlert & Alertmanager

<br>

* alertmanager - start and enable the service

```
systemctl start alertmanager
systemctl enable alertmanager
```

-----------------------------------------------------------------------

# VICTORIA METRICS : VMAlert & Alertmanager

<br>

* a fake smtp

```
docker run -p 8080:80 -p 2525:25 -d --name smtpdev rnwood/smtp4dev
```

-----------------------------------------------------------------------

# VICTORIA METRICS : VMAlert & Alertmanager

<br>

* and you could add karma

```
docker run -d --name karma -p 8081:8080 -e ALERTMANAGER_URI=http://172.17.0.1:9093 ghcr.io/prymitive/karma:latest
```

-----------------------------------------------------------------------

# VICTORIA METRICS : VMAlert & Alertmanager

<br>

* and adapt your vmauth settings

```
  - src_paths:
    - "/alertmanager.*"
    drop_src_path_prefix_parts: 1
    url_prefix:
    - http://vmagent1:9093/
    - http://vmagent2:9093/
```
