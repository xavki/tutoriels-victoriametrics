%title: VictoriaMetrics
%author: xavki


█╗   ██╗██╗ ██████╗████████╗ ██████╗ ██████╗ ██╗ █████╗ 
██║   ██║██║██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗██║██╔══██╗
██║   ██║██║██║        ██║   ██║   ██║██████╔╝██║███████║
╚██╗ ██╔╝██║██║        ██║   ██║   ██║██╔══██╗██║██╔══██║
 ╚████╔╝ ██║╚██████╗   ██║   ╚██████╔╝██║  ██║██║██║  ██║
  ╚═══╝  ╚═╝ ╚═════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚═╝╚═╝  ╚═╝
███╗   ███╗███████╗████████╗██████╗ ██╗ ██████╗███████╗  
████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║██╔════╝██╔════╝  
██╔████╔██║█████╗     ██║   ██████╔╝██║██║     ███████╗  
██║╚██╔╝██║██╔══╝     ██║   ██╔══██╗██║██║     ╚════██║  
██║ ╚═╝ ██║███████╗   ██║   ██║  ██║██║╚██████╗███████║  
╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚═╝ ╚═════╝╚══════╝  
                                                         


-----------------------------------------------------------------------

# VICTORIA METRICS : Cluster Métriques

<br>

https://docs.victoriametrics.com/Cluster-VictoriaMetrics.html#monitoring

-----------------------------------------------------------------------

# VICTORIA METRICS : Cluster Métriques

<br>

```
  - job_name: job_vminsert
    static_configs:
      - targets: 
        - vic1:8480
        - vic2:8480
        - vic3:8480
  - job_name: job_vmselect
    static_configs:
      - targets: 
        - vic1:8481
        - vic2:8481
        - vic3:8481
  - job_name: job_vmstorage
    static_configs:
      - targets: 
        - vic1:8482
        - vic2:8482
        - vic3:8482
```

-----------------------------------------------------------------------

# VICTORIA METRICS : Cluster Métriques


<br>

https://grafana.com/grafana/dashboards/11176-victoriametrics-cluster/

-----------------------------------------------------------------------

# VICTORIA METRICS : Cluster Métriques


<br>

https://github.com/VictoriaMetrics/VictoriaMetrics/tree/master/deployment/docker#alerts

-----------------------------------------------------------------------

# VICTORIA METRICS : Cluster Métriques


<br>

```
https://docs.victoriametrics.com/Single-server-VictoriaMetrics.html#monitoring
```


-----------------------------------------------------------------------

# VICTORIA METRICS : Cluster Métriques


<br>

```
https://docs.victoriametrics.com/Single-server-VictoriaMetrics.html#active-queries
https://docs.victoriametrics.com/Single-server-VictoriaMetrics.html#top-queries
http://192.168.18.51:8481/select/1/
```
