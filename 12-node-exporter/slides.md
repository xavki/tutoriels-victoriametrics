%title: VictoriaMetrics
%author: xavki


█╗   ██╗██╗ ██████╗████████╗ ██████╗ ██████╗ ██╗ █████╗ 
██║   ██║██║██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗██║██╔══██╗
██║   ██║██║██║        ██║   ██║   ██║██████╔╝██║███████║
╚██╗ ██╔╝██║██║        ██║   ██║   ██║██╔══██╗██║██╔══██║
 ╚████╔╝ ██║╚██████╗   ██║   ╚██████╔╝██║  ██║██║██║  ██║
  ╚═══╝  ╚═╝ ╚═════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚═╝╚═╝  ╚═╝
███╗   ███╗███████╗████████╗██████╗ ██╗ ██████╗███████╗  
████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║██╔════╝██╔════╝  
██╔████╔██║█████╗     ██║   ██████╔╝██║██║     ███████╗  
██║╚██╔╝██║██╔══╝     ██║   ██╔══██╗██║██║     ╚════██║  
██║ ╚═╝ ██║███████╗   ██║   ██║  ██║██║╚██████╗███████║  
╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚═╝ ╚═════╝╚══════╝  
                                                         


-----------------------------------------------------------------------

# VICTORIA METRICS : Node exporter

<br>

* global system metrics : disks, network, cpu, ram...

* github : https://github.com/prometheus/node_exporter

<br>

* install the binary

```
VERSION=1.7.0
useradd -rs /bin/false node_exporter
wget -q https://github.com/prometheus/node_exporter/releases/download/v${VERSION}/node_exporter-${VERSION}.linux-amd64.tar.gz 2>&1 > /dev/null
tar -xzf node_exporter-${VERSION}.linux-amd64.tar.gz  2>&1 > /dev/null
mv node_exporter-${VERSION}.linux-amd64/node_exporter /usr/local/bin/
chmod +x /usr/local/bin/node_exporter
```

<br>

* install the systemd service

```
echo "
[Unit]
Description=Node Exporter
After=network-online.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/node_exporter.service
```

<br>

* then start & enable it

```
systemctl enable node_exporter
systemctl restart node_exporter
```

<br>

* add a job to scrape node exporter in vmagent

```
global:
  scrape_interval:     5s 
  evaluation_interval: 5s 
  external_labels:
    datacenter: 'dc1'
scrape_configs:
  - job_name: node_exporter
    static_configs:
      - targets: 
        - <server>:9100
```

<br>

* add a node exporter dashboard

https://grafana.com/grafana/dashboards/1860-node-exporter-full/
