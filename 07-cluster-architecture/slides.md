%title: VictoriaMetrics
%author: xavki


█╗   ██╗██╗ ██████╗████████╗ ██████╗ ██████╗ ██╗ █████╗ 
██║   ██║██║██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗██║██╔══██╗
██║   ██║██║██║        ██║   ██║   ██║██████╔╝██║███████║
╚██╗ ██╔╝██║██║        ██║   ██║   ██║██╔══██╗██║██╔══██║
 ╚████╔╝ ██║╚██████╗   ██║   ╚██████╔╝██║  ██║██║██║  ██║
  ╚═══╝  ╚═╝ ╚═════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚═╝╚═╝  ╚═╝
███╗   ███╗███████╗████████╗██████╗ ██╗ ██████╗███████╗  
████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║██╔════╝██╔════╝  
██╔████╔██║█████╗     ██║   ██████╔╝██║██║     ███████╗  
██║╚██╔╝██║██╔══╝     ██║   ██╔══██╗██║██║     ╚════██║  
██║ ╚═╝ ██║███████╗   ██║   ██║  ██║██║╚██████╗███████║  
╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚═╝ ╚═════╝╚══════╝  
                                                         


-----------------------------------------------------------------------

# VICTORIA METRICS : Déduplication & Downsampling

<br>

Déduplication ???

		* imagine tu veux faire de la haute dispo d'une source pour VM

		* exemple : 2 prometheus, 2 victoria agents...

		* récupération de 2 raw de timeseries identiques :
					même nom de métrique et même label

-----------------------------------------------------------------------

# VICTORIA METRICS : Déduplication & Downsampling

<br>

Déduplication, comment ??

		* options à utiliser : -dedup.minScrapeInterval

		* interval de quoi ? : laps de temps d'évaluation des doublons

		* pour n'en garder que 1 en vertical/horizontal :
					* -dedup.minScrapeInterval=60s
					* si deux sources pendant ce temps = on en garde 1
					* si plusieurs raw, on en garde que 1 (timestamp le plus haut)
					* si même raw et même timestamp = valeur la plus élevée

		* réduction de la volumétrie également après merge tree

			curl http://127.0.0.1:8428/internal/force_merge

-----------------------------------------------------------------------

# VICTORIA METRICS : Déduplication & Downsampling

<br>

Déduplication :

		* mise en oeuvre external_labels identique sur les datasources à dédupliquer

```
global:
  external_labels:
    datacenter: dc1
remote_write:
  - url: "http://vic1:8428/api/v1/write"
```

		* attention à l'interval de scrape
				https://www.robustperception.io/keep-it-simple-scrape_interval-id/

Rq : 
https://docs.victoriametrics.com/?highlight=deduplication#deduplication
https://docs.victoriametrics.com/vmagent.html#scraping-big-number-of-targets

-----------------------------------------------------------------------

# VICTORIA METRICS : Déduplication & Downsampling

<br>

Downsampling :

		* réduction du stockage par échantillonage

```
-downsampling.period=30d:5m,180d:1h
```

		* suivant des tranches de temps

https://docs.victoriametrics.com/?highlight=deduplication#downsampling

-----------------------------------------------------------------------

# VICTORIA METRICS : Déduplication & Downsampling

<br>

Merge Tree :

		* les données de chaque colonne sont stockées séparément

		* les lignes sont classées par clé primaire

		* les blocks sont mergés dans des parts (similaire aux sstables)

		* les parts sont regroupées dans des partitions ordonnées par partition key

