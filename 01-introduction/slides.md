%title: VictoriaMetrics
%author: xavki


█╗   ██╗██╗ ██████╗████████╗ ██████╗ ██████╗ ██╗ █████╗ 
██║   ██║██║██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗██║██╔══██╗
██║   ██║██║██║        ██║   ██║   ██║██████╔╝██║███████║
╚██╗ ██╔╝██║██║        ██║   ██║   ██║██╔══██╗██║██╔══██║
 ╚████╔╝ ██║╚██████╗   ██║   ╚██████╔╝██║  ██║██║██║  ██║
  ╚═══╝  ╚═╝ ╚═════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚═╝╚═╝  ╚═╝
███╗   ███╗███████╗████████╗██████╗ ██╗ ██████╗███████╗  
████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║██╔════╝██╔════╝  
██╔████╔██║█████╗     ██║   ██████╔╝██║██║     ███████╗  
██║╚██╔╝██║██╔══╝     ██║   ██╔══██╗██║██║     ╚════██║  
██║ ╚═╝ ██║███████╗   ██║   ██║  ██║██║╚██████╗███████║  
╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚═╝ ╚═════╝╚══════╝  
                                                         


-----------------------------------------------------------------------                                                                             

# VICTORIA METRICS : introduction - c'est quoi ??

<br>

Base de données TSDB (Time Series Database)

Inspiré de Prometheus et de son écosystème

<br>

Langage : Golang

Collecte, Stockage longue durée

	* Backend pour prometheus (éventuellement)

	* Dispose de son propre agent

VM Community / VM Enterprise

-----------------------------------------------------------------------                                                                             

# VICTORIA METRICS : introduction - c'est quoi ??

<br>

Très hautes performances pour faibles ressources

	* 7x RAM de moins que Prometheus

	* 10x RAM de moins que InfluxDB

	* High Cardinality

	* High Data Compression (70x timescaledb, 7x vs Prometheus, Thanos...)

-----------------------------------------------------------------------                                                                             

# VICTORIA METRICS : introduction - c'est quoi ??

<br>

Support push & pull - sources :

	* exporters prometheus

	* prometheus

	* influxdb

	* graphite

	* OpenTSDB

	* Datadog Agent

	* csv, json

-----------------------------------------------------------------------                                                                             

# VICTORIA METRICS : introduction - c'est quoi ??

<br>

Site officiel : https://victoriametrics.com

Github : https://github.com/VictoriaMetrics/VictoriaMetrics

-----------------------------------------------------------------------                                                                             

# VICTORIA METRICS : introduction - c'est quoi ??

<br>

Installation : 

		* binaire

		* docker

		* paquets apt

		* kubernetes

-----------------------------------------------------------------------                                                                             

# VICTORIA METRICS : introduction - c'est quoi ??

<br>

Entreprises : CERN, Roblox, Wix, Percona, 

Source : https://docs.victoriametrics.com/CaseStudies.html

-----------------------------------------------------------------------                                                                             

# VICTORIA METRICS : introduction - c'est quoi ??

<br>

VictoriaMetrics

		* core database & system

		* monolithe ou services (scaling adapté)

		* services : vmselect / vmstorage / vminsert
	
-----------------------------------------------------------------------                                                                             
# VICTORIA METRICS : introduction - c'est quoi ??

<br>

VictoriaMetrics : core de la TSDB

VMAgent : push / pull datas

VMAlert : alerting

VMCtl : migration de datas (prometheus, influxb, fichiers...)

VMui : interface graphique

VMBackup / VMRestore 

VMGateway / VMAuth

-----------------------------------------------------------------------                                                                             

# VICTORIA METRICS : introduction - c'est quoi ??

<br>

Reprise de la configuration :

	* prometheus

	* alertmanager

	* MetricQL = Promql++

-----------------------------------------------------------------------                                                                             

# VICTORIA METRICS : introduction - c'est quoi ??

<br>

UI :

	* top cardinalités

	* consommation cpu/ram/disk par jobs

	* templates

	* dashboards

-----------------------------------------------------------------------                                                                             

# VICTORIA METRICS : introduction - c'est quoi ??

<br>

VictoriaLogs

LogsQL

ça sent bon pour les années à venir !!!
