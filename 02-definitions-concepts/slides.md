%title: VictoriaMetrics
%author: xavki


█╗   ██╗██╗ ██████╗████████╗ ██████╗ ██████╗ ██╗ █████╗ 
██║   ██║██║██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗██║██╔══██╗
██║   ██║██║██║        ██║   ██║   ██║██████╔╝██║███████║
╚██╗ ██╔╝██║██║        ██║   ██║   ██║██╔══██╗██║██╔══██║
 ╚████╔╝ ██║╚██████╗   ██║   ╚██████╔╝██║  ██║██║██║  ██║
  ╚═══╝  ╚═╝ ╚═════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚═╝╚═╝  ╚═╝
███╗   ███╗███████╗████████╗██████╗ ██╗ ██████╗███████╗  
████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║██╔════╝██╔════╝  
██╔████╔██║█████╗     ██║   ██████╔╝██║██║     ███████╗  
██║╚██╔╝██║██╔══╝     ██║   ██╔══██╗██║██║     ╚════██║  
██║ ╚═╝ ██║███████╗   ██║   ██║  ██║██║╚██████╗███████║  
╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚═╝ ╚═════╝╚══════╝  
                                                         


-----------------------------------------------------------------------

# VICTORIA METRICS : Définitions & Concepts

<br>

TimeSeries / Série Temporelle

	* métrique

	* clef / valeur (numérique) temporel

	* la valeur évolue dans le temps
		ex :
			load_average 1.02 1483228830000
			load_average 1.51 1483228850000

-----------------------------------------------------------------------

# VICTORIA METRICS : Définitions & Concepts

<br>

TimeSeries / Série Temporelle

	* objectif :
			observer
			alerter
			corréler

	* standards : openmetrics

		https://github.com/OpenObservability/OpenMetrics/

		metric{label=valeur_label}  value   timestamp_epoch

	* Objectifs TSDB = agrégation = distributed column store


-----------------------------------------------------------------------

# VICTORIA METRICS : Définitions & Concepts

<br>

Labels

	* spécifier la métrique

	* permettre de filtrer

	* ex : load_average{hostname="vm1"} 1.02 1483228830000

	* mais aussi

```
requests_total{path="/", code="200"} 
{__name__="requests_total", path="/", code="200"} 
```

-----------------------------------------------------------------------

# VICTORIA METRICS : Définitions & Concepts

<br>

Labels


	* une time series c'est la combinaison d'une métrique et de ses labels

```
requests_total{path="/", code="404"} 100
requests_total{path="/", code="200"} 10
requests_total{path="/check", code="200"} 1001
```

PS : chaque ligne est un raw sample


-----------------------------------------------------------------------

# VICTORIA METRICS : Définitions & Concepts

<br>

Cardinalités

	* le nombre de time series unique

	* démultiplication par les labels

```
requests_total{path="/", code="404"} 100
requests_total{path="/", code="200"} 10
requests_total{path="/check", code="200"} 1001
```

	* fort impact sur la consommation de ressources (mémoire)

-----------------------------------------------------------------------

# VICTORIA METRICS : Définitions & Concepts

<br>

Type métriques 

	* gauge : valeur numérique à variation positive et/ou négative
			ex : utilisation direct de la valeur

	* counter : valeur numérique à variation positive / incrémentée
			ex : utilisation de la dérivé (variation dans le temps)

-----------------------------------------------------------------------

# VICTORIA METRICS : Définitions & Concepts

<br>

Type métriques 

	* histogram : représente une distribution stockée dans un bucket

```
# TYPE http_request_duration_seconds histogram
http_request_duration_seconds_bucket{le="0.1"} 300
http_request_duration_seconds_bucket{le="0.5"} 400
http_request_duration_seconds_bucket{le="1.0"} 500
http_request_duration_seconds_bucket{le="5.0"} 700
http_request_duration_seconds_bucket{le="+Inf"} 1000
http_request_duration_seconds_sum 15000
http_request_duration_seconds_count 10000
```

-----------------------------------------------------------------------

# VICTORIA METRICS : Définitions & Concepts

<br>

Type métriques 

	* summary : idem histogram mais spécifiquement pour les quantiles

```
go_gc_duration_seconds{quantile="0"} 0
go_gc_duration_seconds{quantile="0.25"} 0
go_gc_duration_seconds{quantile="0.5"} 0
go_gc_duration_seconds{quantile="0.75"} 8.0696e-05
go_gc_duration_seconds{quantile="1"} 0.001222168
go_gc_duration_seconds_sum 0.015077078
go_gc_duration_seconds_count 83
```

-----------------------------------------------------------------------

# VICTORIA METRICS : Définitions & Concepts

<br>

Push / Pull Model / Scrape

	*	VM / Prometheus... pull modèle

	* va chercher la donnée par scrape

	* scrape 
			page d'openmetrics
			exposé sur une ip/dns, port
			route (/metrics)
			fréquence (secondes/minutes/heures...)
			targets
			risque : perte de valeurs

	* (push modèle possible avec VM)

-----------------------------------------------------------------------

# VICTORIA METRICS : Définitions & Concepts

<br>

Exporters

	* exposent des routes métriques

	* pour le compte d'autres applications
		ex : elasticsearch, mysql...

	* si les applications ne fournissent pas déjà cette route

-----------------------------------------------------------------------

# VICTORIA METRICS : Définitions & Concepts

<br>

Service Discovery

	* fournir un outil de registry
		ex : consul, api de cloud provider, serveur dns, api kubernetes...

	* modification dynamique de la configuration

	* indispensable pour les infra dynamique (ou pas :) )

	* généralement bien plus pratique que d'éditer la conf par ansible, puppet...

	* possibilité de réécrire les labels ou ajouter des labels du service de discovery

-----------------------------------------------------------------------

# VICTORIA METRICS : Définitions & Concepts

<br>

Remote Write

	* backend de stockage longue durée (ex : VM, thanos pour prometheus)

	* forte volumétrie

	* longue durée
	
-----------------------------------------------------------------------

# VICTORIA METRICS : Définitions & Concepts

<br>

Déduplication

	* capacité à éviter les doublons de métriques
		ex : 	multiple prometheus pour simuler une haute dispo
					puis envoi dans le backend qui dédup

-----------------------------------------------------------------------

# VICTORIA METRICS : Définitions & Concepts

<br>

Downsampling

	* échantillonage des métriques

	* réduction de la volumétrie

	* conservation sur longue durée

	* exploitation sur large échelle de temps

-----------------------------------------------------------------------

# VICTORIA METRICS : Définitions & Concepts

<br>

Compression : delta / double-delta / bit / hash...

			load_average 1.02 1483228830000
			load_average 1.51 1483228850000	+0.49
			load_average 1.52 1483228870000 +0.01
			load_average 1.65 1483228890000 +0.13

-----------------------------------------------------------------------

# VICTORIA METRICS : Définitions & Concepts

<br>

Plus sur : 
		https://docs.victoriametrics.com/keyConcepts.html
