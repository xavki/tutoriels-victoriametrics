%title: VictoriaMetrics
%author: xavki


█╗   ██╗██╗ ██████╗████████╗ ██████╗ ██████╗ ██╗ █████╗ 
██║   ██║██║██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗██║██╔══██╗
██║   ██║██║██║        ██║   ██║   ██║██████╔╝██║███████║
╚██╗ ██╔╝██║██║        ██║   ██║   ██║██╔══██╗██║██╔══██║
 ╚████╔╝ ██║╚██████╗   ██║   ╚██████╔╝██║  ██║██║██║  ██║
  ╚═══╝  ╚═╝ ╚═════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚═╝╚═╝  ╚═╝
███╗   ███╗███████╗████████╗██████╗ ██╗ ██████╗███████╗  
████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║██╔════╝██╔════╝  
██╔████╔██║█████╗     ██║   ██████╔╝██║██║     ███████╗  
██║╚██╔╝██║██╔══╝     ██║   ██╔══██╗██║██║     ╚════██║  
██║ ╚═╝ ██║███████╗   ██║   ██║  ██║██║╚██████╗███████║  
╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚═╝ ╚═════╝╚══════╝  
                                                         


-----------------------------------------------------------------------

# VICTORIA METRICS : Cluster core

<br>

* some variables

```
VERSION="1.95.1"
IP=$(hostname -I | awk '{print $2}')
USER_APP=victoriametrics
INSERTER_DNS="127.0.0.1:8427"
```

-----------------------------------------------------------------------

# VICTORIA METRICS : Cluster core

<br>


* install binaries

```
wget -qq https://github.com/VictoriaMetrics/VictoriaMetrics/releases/download/v${VERSION}/vmutils-linux-amd64-v${VERSION}.tar.gz
tar xzf vmutils-linux-amd64-v${VERSION}.tar.gz -C /usr/local/bin/
chmod -R +x /usr/local/bin/

groupadd --system ${USER_APP}
useradd -s /sbin/nologin --system -g ${USER_APP} ${USER_APP}

mkdir -p mkdir -p /etc/vmagent/ /var/lib/vmagent/ /etc/vmauth/
chown -R ${USER_APP}:${USER_APP} /etc/vmagent/ /var/lib/vmagent/ /etc/vmauth/
```


-----------------------------------------------------------------------

# VICTORIA METRICS : Cluster core

<br>

* install vmagent systemd service

```
echo "
[Unit]
Description=Description=VMAgent service
After=network.target

[Service]
Type=simple
User=${USER_APP}
ExecStart=/usr/local/bin/vmagent-prod \
      -promscrape.config=/etc/vmagent/config.yml \
      -remoteWrite.url=http://${INSERTER_DNS}/api/v1/write \
      -promscrape.config.strictParse=false \
      -remoteWrite.tmpDataPath=/var/lib/vmagent/

SyslogIdentifier=vmagent
Restart=always

[Install]
WantedBy=multi-user.target
" >/etc/systemd/system/vmagent.service
```

-----------------------------------------------------------------------

# VICTORIA METRICS : Cluster core

<br>

* install vmauth systemd service

```
echo "
[Unit]
Description=Description=VMAuth service
After=network.target

[Service]
Type=simple
User=${USER_APP}
ExecStart=/usr/local/bin/vmauth-prod \
      -configCheckInterval=5s \
      -auth.config=/etc/vmauth/config.yml

SyslogIdentifier=vmauth
Restart=always

[Install]
WantedBy=multi-user.target
" >/etc/systemd/system/vmauth.service
```


-----------------------------------------------------------------------

# VICTORIA METRICS : Cluster core

<br>

* vmagent scrape configuration

```
echo "
global:
  scrape_interval:     5s 
  evaluation_interval: 5s 
  external_labels:
    datacenter: 'dc1'
rule_files:
scrape_configs:
  - job_name: node_exporter
    static_configs:
      - targets: 
" > /etc/vmagent/config.yml

awk '$1 ~ "^192.168" {print "        - "$2":9100"}' /etc/hosts >> /etc/vmagent/config.yml
```


-----------------------------------------------------------------------

# VICTORIA METRICS : Cluster core

<br>

* vmauth configuration (pathcs/backends)

```
echo '
unauthorized_user:
  url_map:
  - src_paths:
    - /targets
    - /static.+
    - /service-discovery
    - /target-relabel-debug
    url_prefix:
    - http://vmagent1:8429
    - http://vmagent2:8429
  - src_paths:
    - /api/v1/write
    url_prefix:
    - http://vic1:8480/insert/1/prometheus
    - http://vic2:8480/insert/1/prometheus
    - http://vic3:8480/insert/1/prometheus
  - src_paths:
    - /api/v1/series
    - /api/v1/query
    - /api/v1/query_range
    - /api/v1/label/[^/]+/values
    url_prefix:
    - http://vic1:8481/select/1/prometheus
    - http://vic2:8481/select/1/prometheus
    - http://vic3:8481/select/1/prometheus
  - src_paths:
    - "/vmui.+"
    - "/prometheus.+"
    url_prefix:
    - http://vic1:8481/select/1
    - http://vic2:8481/select/1
    - http://vic3:8481/select/1
' > /etc/vmauth/config.yml
```


-----------------------------------------------------------------------

# VICTORIA METRICS : Cluster core

<br>

* start and enable all

```
systemctl restart vmagent
systemctl enable vmagent

systemctl restart vmauth
systemctl enable vmauth
```
