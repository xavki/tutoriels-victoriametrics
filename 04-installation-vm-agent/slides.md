%title: VictoriaMetrics
%author: xavki


█╗   ██╗██╗ ██████╗████████╗ ██████╗ ██████╗ ██╗ █████╗ 
██║   ██║██║██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗██║██╔══██╗
██║   ██║██║██║        ██║   ██║   ██║██████╔╝██║███████║
╚██╗ ██╔╝██║██║        ██║   ██║   ██║██╔══██╗██║██╔══██║
 ╚████╔╝ ██║╚██████╗   ██║   ╚██████╔╝██║  ██║██║██║  ██║
  ╚═══╝  ╚═╝ ╚═════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚═╝╚═╝  ╚═╝
███╗   ███╗███████╗████████╗██████╗ ██╗ ██████╗███████╗  
████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║██╔════╝██╔════╝  
██╔████╔██║█████╗     ██║   ██████╔╝██║██║     ███████╗  
██║╚██╔╝██║██╔══╝     ██║   ██╔══██╗██║██║     ╚════██║  
██║ ╚═╝ ██║███████╗   ██║   ██║  ██║██║╚██████╗███████║  
╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚═╝ ╚═════╝╚══════╝  
                                                         


-----------------------------------------------------------------------

# VICTORIA METRICS : Installation de VMAgent

<br>



* node-exporter : mise ne place du binaire

```
VERSION="1.6.1"
useradd -rs /bin/false node_exporter
wget https://github.com/prometheus/node_exporter/releases/download/v${VERSION}/node_exporter-${VERSION}.linux-amd64.tar.gz
tar -xvzf node_exporter-${VERSION}.linux-amd64.tar.gz
mv node_exporter-${VERSION}.linux-amd64/node_exporter /usr/local/bin/
chown node_exporter:node_exporter /usr/local/bin/node_exporter```

* node-exporter : création du service systemd

```
[Unit]
Description=Node Exporter
After=network-online.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
```

* node-exporter : start & enable du service

```
systemctl enable node_exporter
systemctl restart node_exporter
```

* victoria-agent : user & directory

```
groupadd --system victoriagent
useradd -s /sbin/nologin --system -g victoriagent victoriagent
mkdir -p mkdir -p /etc/victoria-agent/conf/
chown -R victoriagent:victoriagent /etc/victoria-agent
```

* victoria-agent : mise en place du binaire

```
wget -qq https://github.com/VictoriaMetrics/VictoriaMetrics/releases/download/v1.91.3/vmutils-linux-amd64-v1.91.3.tar.gz
tar xzf vmutils-linux-amd64-v1.91.3.tar.gz -C /usr/local/bin/
chmod -R +x /usr/local/bin/
```

* victoria-agent : service systemd

```
[Unit]
Description=Description=VictoriaAgent service
After=network.target

[Service]
Type=simple
#User=victoriagent
#Group=victoriagent
ExecStart=/usr/local/bin/vmagent-prod \
       -promscrape.config=/etc/victoria-agent/conf/victoria-agent.yml \
       -remoteWrite.url=http://vic1:8428/api/v1/write \
       -promscrape.config.strictParse=false

SyslogIdentifier=victoriagent
Restart=always

[Install]
WantedBy=multi-user.target
```

* victoria-agent : configuration

```
global:
  scrape_interval:     5s 
  evaluation_interval: 5s 
rule_files:
scrape_configs:
  - job_name: node_exporter
    static_configs:
      - targets:
        - node1:9100
        - node2:9100
        - vic1:9100
```

* victoria-agent : start & enable

```
systemctl restart victoriagent
systemctl enable victoriagent
```
