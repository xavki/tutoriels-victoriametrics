%title: VictoriaMetrics
%author: xavki


█╗   ██╗██╗ ██████╗████████╗ ██████╗ ██████╗ ██╗ █████╗ 
██║   ██║██║██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗██║██╔══██╗
██║   ██║██║██║        ██║   ██║   ██║██████╔╝██║███████║
╚██╗ ██╔╝██║██║        ██║   ██║   ██║██╔══██╗██║██╔══██║
 ╚████╔╝ ██║╚██████╗   ██║   ╚██████╔╝██║  ██║██║██║  ██║
  ╚═══╝  ╚═╝ ╚═════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚═╝╚═╝  ╚═╝
███╗   ███╗███████╗████████╗██████╗ ██╗ ██████╗███████╗  
████╗ ████║██╔════╝╚══██╔══╝██╔══██╗██║██╔════╝██╔════╝  
██╔████╔██║█████╗     ██║   ██████╔╝██║██║     ███████╗  
██║╚██╔╝██║██╔══╝     ██║   ██╔══██╗██║██║     ╚════██║  
██║ ╚═╝ ██║███████╗   ██║   ██║  ██║██║╚██████╗███████║  
╚═╝     ╚═╝╚══════╝   ╚═╝   ╚═╝  ╚═╝╚═╝ ╚═════╝╚══════╝  
                                                         


-----------------------------------------------------------------------

# VICTORIA METRICS : Installation & Grafana

<br>

* User & Directory

```
groupadd --system victoriametrics
useradd -s /sbin/nologin --system -g victoriametrics victoriametrics

mkdir -p /var/lib/victoria-metrics/
chown victoriametrics:victoriametrics /var/lib/victoria-metrics/
```

-----------------------------------------------------------------------

# VICTORIA METRICS : Installation

<br>

* installation de l'archive


```
wget -qq https://github.com/VictoriaMetrics/VictoriaMetrics/releases/download/v1.91.3/victoria-metrics-linux-amd64-v1.91.3.tar.gz
tar xzf victoria-metrics-linux-amd64-v1.91.3.tar.gz -C /usr/local/bin/
chmod +x /usr/local/bin/victoria-metrics-prod
```

-----------------------------------------------------------------------

# VICTORIA METRICS : Installation

<br>

* création du service systemd & cli

```
echo "
[Unit]
Description=Description=VictoriaMetrics service
After=network.target

[Service]
Type=simple
LimitNOFILE=2097152
User=victoriametrics
Group=victoriametrics
ExecStart=/usr/local/bin/victoria-metrics-prod \
       -storageDataPath=/var/lib/victoria-metrics/ \
       -httpListenAddr=0.0.0.0:8428 \
       -retentionPeriod=1
#1 Month

SyslogIdentifier=victoriametrics
Restart=always

PrivateTmp=yes
ProtectHome=yes
NoNewPrivileges=yes
ProtectSystem=full

[Install]
WantedBy=multi-user.target
" >/etc/systemd/system/victoriametrics.service
```

-----------------------------------------------------------------------

# VICTORIA METRICS : Installation

<br>

* démarrage de victoriametrics

```
systemctl restart victoriametrics
systemctl enable victoriametrics
```

-----------------------------------------------------------------------

# VICTORIA METRICS : Installation

<br>

* vérification via une requête

```
curl http://127.0.0.1:8428/api/v1/query -d 'query={job=~".*"}'
```

-----------------------------------------------------------------------

# VICTORIA METRICS : Installation

<br>

* installation de grafana

```
sudo wget -q -O - https://packages.grafana.com/gpg.key | apt-key add -
sudo add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"
apt-get update -qq >/dev/null
apt-get install -qq -y grafana >/dev/null
```

<br>

* ajout de la source victoriametrics

```
echo "
datasources:
-  access: 'proxy'
   editable: true 
   is_default: true
   name: 'prometheus'
   org_id: 1 
   type: 'prometheus' 
   url: 'http://192.168.18.51:9090' 
   version: 1
" > /etc/grafana/provisioning/datasources/all.yml
chmod 644 /etc/grafana/provisioning/datasources/all.yml
```
